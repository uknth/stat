package main

import (
	"fmt"
	"github.com/dlintw/goconf" // for configuration file
	"io"
	"log"
	"net/smtp"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

//define io writer for log.
var logfile io.Writer
var stat *goconf.ConfigFile
var comm *goconf.ConfigFile
var subject = "Server Report"
var content string

func main() {
	fo, _ := os.Create("/var/tmp/stat.log")
	logfile = io.Writer(fo)
	log.SetOutput(logfile)
	if !checkConf() {
		log.Fatal("Configuration file not found.")
	}

	//Open commands from command_conf.ini
	t := value("default", "timeout", stat)
	timeunit := value("default", "timeUnit", stat)
	timeout, err := strconv.Atoi(t)
	if err != nil {
		log.Fatal("Error in Timeout, please check.")
	}
	for {
		command()
		mail(subject, content)
		switch timeunit {
		case "DAY":
			timeout = timeout * 24
			time.Sleep(time.Duration(timeout) * time.Hour)
		case "MONTH":
			timeout = timeout * 24 * 30
			time.Sleep(time.Duration(timeout) * time.Hour)
		case "WEEK":
			timeout = timeout * 24 * 7
			time.Sleep(time.Duration(timeout) * time.Hour)
		case "HOUR":
			time.Sleep(time.Duration(timeout) * time.Hour)
		case "MIN":
			if timeout < 5 {
				log.Fatal("Minimum timeout should not be less than 5 min.")
			}
			time.Sleep(time.Duration(timeout) * time.Minute)
		default:
			log.Fatal("Please check timeunit")
		}
	}

}

func command() {
	// load all the commands mentioned in command_conf.in
	commands := strings.Split(value("default", "commands", comm), ",")
	for _, val := range commands {
		//log.Print("Executing: " + value(val, "path", comm))
		// check if custom command is there. 		
		var cmd *exec.Cmd
		if value(val, "custom", comm) == "true" {
			cmd = exec.Command("bash", "./shell/"+val)
		} else {
			cmd = exec.Command(value(val, "path", comm))
			cmd.Args = strings.Split(value(val, "arg", comm), " ")
		}
		out, err := cmd.CombinedOutput()
		if value("default", "msgtype", stat) == "html" {
			content += "<h3>" + val + "</h3>"
			content += strings.Replace(
				strings.Replace(
					string(out),
					"\n", "<br/>", -1),
				" ", "&nbsp;", -1) + "<br/>"
		} else {
			content += val + "\n -----------------------------------------\n"
			content += string(out) + "\n\n"
		}
		//log.Print(content)
		if err != nil {
			log.Print(err)
		}
	}
}

func checkConf() bool {
	pwd, err := os.Getwd()
	if err != nil {
		fmt.Println(err.Error())
		return false
	}
	path := pwd + "/" + "stat_conf.ini"
	_, err = os.Stat(path)
	if err != nil {
		fmt.Println(err.Error())
		return false
	}
	// Open stat_conf.ini
	stat, err = goconf.ReadConfigFile(path)
	if err != nil {
		log.Panic(err)
	}
	path = pwd + "/" + "command_conf.ini"
	_, err = os.Stat(path)
	if err != nil {
		fmt.Println(err.Error())
		return false
	}
	comm, err = goconf.ReadConfigFile(path)
	return true
}

func value(section string, option string, conf *goconf.ConfigFile) string {
	value, err := conf.GetRawString(section, option)
	if err != nil {
		log.Panic(err)
		return "FAIL"
	}
	return value
}

func mail(subject string, content string) bool {
	mailids := strings.Split(value("default", "email", stat), ",")
	server := value("default", "server", stat)
	port := value("default", "port", stat)
	fromid := value("default", "fromid", stat)
	frompass := value("default", "frompass", stat)
	length := len(mailids) - 1
	semaphore := make(chan int)
	count := 0
	for _, val := range mailids {
		go func(
			server string,
			port string,
			fromid string,
			frompass string,
			toid string,
			sub string,
			content string,
			semaphore chan int) {
			var msg []byte
			//log.Print("sending mail to " + toid)
			mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
			subject := "Subject: " + sub + "\n"
			if value("default", "msgtype", stat) == "html" {
				msg = []byte(subject + mime + "<html><body>" + content + "</body></html>")
			} else {
				msg = []byte(subject + content)
			}
			auth := smtp.PlainAuth("", fromid, frompass, server)
			smtp.SendMail(server+":"+port, auth, fromid, []string{toid}, msg)
			semaphore <- 1
		}(server, port, fromid, frompass, val, subject, content, semaphore)
		if count >= length {
			<-semaphore
		} else {
			count++
		}
	}
	return true

}
